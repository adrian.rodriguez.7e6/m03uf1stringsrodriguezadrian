﻿/*
 Author:  Adrian Rodriguez
 Date:8/12/2022
 Description: Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals.
*/


using System;
using System.ComponentModel;
using System.Linq;

namespace Strings
{
    internal class Strings
    {
        // Et diu si els dos strings son iguals
        public void SonIguals()
        {
            Console.WriteLine("Introdueix la primera seqüencia de caracters.");
            string seqüencia1 = Console.ReadLine();
            Console.WriteLine("Introdueix la primera seqüencia de caracters.");
            string seqüencia2 = Console.ReadLine(); 
            if (seqüencia1.CompareTo(seqüencia2)==0)
            {
                Console.WriteLine("Són iguals");
            }
            else
            {
                Console.WriteLine("No són iguals");
            }
        }

        // Et diu si els dos strings son iguals sense importar las majuscules
        public void SonIguals2()
        {
            Console.WriteLine("Introdueix la primera seqüencia de caracters.");
            string seqüencia1 = Console.ReadLine();
            Console.WriteLine("Introdueix la primera seqüencia de caracters.");
            string seqüencia2 = Console.ReadLine();
            
            if (seqüencia1.ToUpper().CompareTo(seqüencia2.ToUpper()) == 0)
            {
                Console.WriteLine("Són iguals");
            }
            else
            {
                Console.WriteLine("No són iguals");
            }
        }
        // Elimina letras de un string
        public void PurgaDeCaracters()
        {
            Console.WriteLine("Introdueix la seqüencia de caracters.");
            string seqüencia = Console.ReadLine();
            string caracter;
            do
            {
                Console.WriteLine("Introdueix un caracter que vulguis treure de la seqüencia.");
                caracter = Console.ReadLine();
                
                seqüencia = seqüencia.Replace(caracter,"");
            } while (caracter!="0");
            Console.WriteLine(seqüencia);
        }
        // Substituir lletres del string
        public void SubstitueixElCaracter()
        {
            Console.WriteLine("Introdueix la seqüencia de caracters.");
            string seqüencia = Console.ReadLine();
            Console.WriteLine("Introdueix la lletra que vols canviar.");
            string caracter1 = Console.ReadLine();
            Console.WriteLine("Per quina lletra la vols canviar?.");
            string caracter2 = Console.ReadLine();
            seqüencia = seqüencia.Replace(caracter1, caracter2);
            Console.WriteLine(seqüencia);
        }
        // Mostra la distancia d'Hamming
        public void DistanciaDeHamming()
        {
            string ADN1;
            string ADN2;
            do
            {
                Console.WriteLine("Introduce una cadena de ADN");
                ADN1 = Console.ReadLine();
                Console.WriteLine("Introduce una cadena de ADN");
                ADN2 = Console.ReadLine();
                if (ADN1.Length != ADN2.Length)
                {
                    Console.WriteLine("La mida de les dues cadenes és diferent.");
                }
            } while (ADN1.Length!=ADN2.Length);
            
            char[] charADN1 = ADN1.ToCharArray();
            char[] charADN2 = ADN2.ToCharArray();
            int contador = 0;

            for(int i=0; i < charADN1.Length; i++)
            {
                if (charADN1[i] != charADN2[i])
                {
                    contador++;
                }
            }

            Console.WriteLine($"La distancia d'Hamming és de {contador}");

        }
        // Diu si la secuencia de parentesis esta ben escrita.
        public void Parentesis()
        {
            Console.WriteLine("Introduce una cadena de parentesis");
            string parentesis = Console.ReadLine();
            int contador = 0;   
            foreach (char val in parentesis)
            {
                if(val == '(')
                {
                    contador++;
                }
                if(val == ')')
                {
                    contador--;
                }
            }
            char[] charParentesis = parentesis.ToCharArray();
            if (contador==0 && charParentesis[0] == '(' && charParentesis[charParentesis.Length-1] == ')')
            {
                Console.WriteLine("La secuencia esta ben escrita.");
            }
            else
            {
                Console.WriteLine("La secuencia esta mal escrita.");
            }
        }
        // Dir si la paraula es un palindrom
        public void Palindrom()
        {
            Console.WriteLine("Introdueix una paraula.");
            string paraula = Console.ReadLine();
            char[] charParaula = paraula.ToCharArray();
            char[] reverse = paraula.ToCharArray();
            Array.Reverse(reverse);
            bool palindrom = true;
            int i=0;
            foreach(char val in reverse)
            {
                if (val != charParaula[i])
                {
                    palindrom = false;
                }
                i++;
            }
            if (palindrom == true)
            {
                Console.WriteLine("És un palindrom");
            }
            else
            {
                Console.WriteLine("No és un palindrom");
            }
        }
        // Et printa per pantalla les paraules invertides.
        public void InverteixLesParaules()
        {
            Console.WriteLine("Introdueix una paraula. Cuando quieras acabar introduce bye.");
            string paraula;
            do
            {
                paraula = Console.ReadLine();
                char[] charParaula = paraula.ToCharArray();
                Array.Reverse(charParaula);

                foreach (char val in charParaula)
                {
                    Console.Write(val);
                }
                Console.WriteLine("");    
            } while (paraula!="bye");
        }
        // Invertir les paraules duna frase.
        public void InverteixLesParaules2()
        {
            Console.WriteLine("Escriu una frase");
            string frase = Console.ReadLine();
            string[] paraula = frase.Split(" ");
            string fraseInversa = "";
            for (int i=paraula.Length-1; i>=0; i--)
            {
                fraseInversa+= paraula[i]+" ";
            }
            Console.WriteLine(fraseInversa);
        }
        // Si esta escrit hola dins la cadena de caracters et retorna "Hola", si no et retorna "Adeu".
        public void HolaIAdeu()
        {
            Console.WriteLine("Introdueix una cadena de caracters.");
            string cadena = Console.ReadLine();
            char[] charCadena = cadena.ToCharArray();
            bool hola = false;
            for (int i=0; i<=charCadena.Length-3;i++)
            {
                if (charCadena[i] == 'h' && charCadena[i+1] == 'o' && charCadena[i+2] == 'l' && charCadena[i+3] == 'a')
                {
                    hola = true;
                }
            }
            if(hola==true)
            {
                Console.WriteLine("Hola");
            }
            else
            {
                Console.WriteLine("Adeu");
            }
        }
        
        public bool Menu(bool end)
        {
            string[] exercicis = { " 0 - Exit", " 1 - SonIguals", " 2 - SonIguals2", " 3 - PurgaDeCaracters",
                " 4 - SubstitueixElCaracter", " 5 - DistanciaDeHamming", " 6 - Parentesis", " 7 - Palindrom",
                " 8 - InverteixLesParaules", " 9 - InverteixLesParaules2", "10 - HolaIAdeu"};
            Console.WriteLine("Quin exercici vols executar?\n");
            for (int i = 0; i < 11; i++)
            {
                Console.WriteLine(exercicis[i]);
            }
            Console.Write("\n Escriu un número: ");
            string option = Console.ReadLine();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    SonIguals();
                    break;
                case "2":
                    SonIguals2();
                    break;
                case "3":
                    PurgaDeCaracters();
                    break;
                case "4":
                    SubstitueixElCaracter();
                    break;
                case "5":
                    DistanciaDeHamming();
                    break;
                case "6":
                    Parentesis();
                    break;
                case "7":
                    Palindrom();
                    break;
                case "8":
                    InverteixLesParaules();
                    break;
                case "9":
                    InverteixLesParaules2();
                    break;
                case "10":
                    HolaIAdeu();
                    break;
                default:
                    Console.WriteLine("\nOpció incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }
        static void Main()
        {
            var menu = new Strings();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
    }
}
